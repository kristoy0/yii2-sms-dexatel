<?php

namespace kristoy0\sms\dexatel;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use mikk150\sms\BaseProvider;
use mikk150\sms\MessageInterface;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Provider extends BaseProvider
{
    protected const API_KEY_HEADER = 'X-Dexatel-Key';
    protected const CHANNEL_SMS = 'SMS';
    protected const STATUS_FAILED = 'failed';

    /**
     * @inheritDoc
     */
    public $messageClass = Message::class;

    /**
     * @var string
     */
    public $baseUrl = 'https://api.dexatel.com/v1/';

    /**
     * @var int
     */
    public $timeout = 10;

    /**
     * @var ?Client
     */
    private $_client;

    /**
     * API Key
     *
     * @var string
     */
    public $apiKey;

    /**
     * @inheritDoc
     */
    public function init()
    {
        if (!$this->apiKey) {
            throw new InvalidConfigException('Missing required parameter "apiKey" when instantiating "' . __CLASS__ . '".');
        }

        parent::init();
    }

    /**
     * @inheritDoc
     */
    public function sendMessage($message)
    {
        $response = $this->getClient()->post('messages', [
            RequestOptions::HEADERS => [
                self::API_KEY_HEADER => $this->apiKey,
            ],
            RequestOptions::JSON => $this->getData($message),
        ])
            ->getBody()
            ->getContents();

        $decodedResponse = Json::decode($response);
        $data = ArrayHelper::getValue($decodedResponse, 'data');
        $isSuccess = !ArrayHelper::isIn(self::STATUS_FAILED, ArrayHelper::getColumn($data, 'status'));

        return $isSuccess;
    }

    /**
     * Gets request data
     *
     * @param MessageInterface $message
     *
     * @return array
     */
    protected function getData(MessageInterface $message)
    {
        return [
            'data' => [
                'from' => $message->getFrom(),
                'to' => $message->getTo(),
                'text' => $message->getBody(),
                'channel' => self::CHANNEL_SMS,
            ],
        ];
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client)
    {
        $this->_client = $client;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        if ($this->_client === null) {
            $this->_client = new Client([
                'timeout' => $this->timeout,
                'base_uri' => $this->baseUrl
            ]);
        }

        return $this->_client;
    }
}
