<?php

namespace kristoy0\sms\dexatel;

use mikk150\sms\BaseMessage;

class Message extends BaseMessage
{
    /**
     * @var string
     */
    private $_from;

    /**
     * @var string|array
     */
    private $_to;

    /**
     * @var string
     */
    private $_body;

    /**
     * @inheritDoc
     */
    public function getFrom()
    {
        return $this->_from;
    }

    /**
     * @inheritDoc
     */
    public function setFrom($from)
    {
        $this->_from = $from;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getBody()
    {
        return $this->_body;
    }

    /**
     * @inheritDoc
     */
    public function setBody($body)
    {
        $this->_body = $body;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getTo()
    {
        return is_array($this->_to) ? $this->_to : [$this->_to];
    }

    /**
     * @inheritDoc
     */
    public function setTo($to)
    {
        $this->_to = $to;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function toString()
    {
        return $this->_body;
    }
}
