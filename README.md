# Yii2 Dexatel SMS Provider

Yii2 wrapper for [Dexatel](https://dexatel.com/) sms provider.

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```bash
$ composer require kristoy0/yii2-sms-dexatel "^1.0.0"
```

or add

```
"kristoy0/yii2-sms-dexatel": "^1.0.0"
```

to the ```require``` section of your `composer.json` file.

## Configuration

### Via application component
```php
'components' => [
    'sms' => [
        'class' => kristoy0\sms\dexatel\Provider::class,
        'apiKey' => '<your dexatel API key>',
    ],
],
```
## Usage

```php
Yii::$app
    ->sms
    ->compose('Text to send', ['attribute' => 'value'])
    ->setTo('123412341234')
    ->setFrom('Sender')
    ->send();
```
