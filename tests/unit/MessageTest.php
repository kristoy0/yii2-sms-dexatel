<?php

namespace tests\unit;

use Codeception\Test\Unit;
use kristoy0\sms\dexatel\Message;

class MessageTest extends Unit
{
    public function testFrom()
    {
        $message = new Message();

        $message->setFrom('123456');

        $this->assertEquals('123456', $message->getFrom());
    }

    public function testToString()
    {
        $message = new Message();

        $message->setBody('123456');

        $this->assertEquals('123456', $message->toString());
    }

    public function testTo()
    {
        $message = new Message();

        $message->setTo(123);

        $this->assertEquals([123], $message->getTo());
    }

    public function testToWithArray()
    {
        $message = new Message();

        $message->setTo([123, 456]);

        $this->assertEquals([123, 456], $message->getTo());
    }
}
