<?php

namespace tests\unit;

use Codeception\Test\Unit;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use kristoy0\sms\dexatel\Provider;
use yii\base\InvalidConfigException;
use yii\helpers\Json;

class ProviderTest extends Unit
{
    public function testAcceptedStatus()
    {
        $provider = new Provider([
            'apiKey' => 'hurrdurr'
        ]);

        $mock = new MockHandler([
            new Response(201, [], Json::encode([
                'data' => [
                    [
                        'id' => '2e214e92-4d0f-4ec4-9952-583d086582d6',
                        'account_id' => '72e47e47-95b9-41c2-bdcd-55dd9bb04e14',
                        'text' => 'Welcome to Dexatel\'s API',
                        'from' => 'Dexatel',
                        'to' => '17088407149',
                        'channel' => 'SMS',
                        'status' => 'delivered',
                        'segments' => 1,
                        'encoding' => 'GSM-7'
                    ],
                    [
                        'id' => '2e214e92-4d0f-4ec4-9952-583d086582d6',
                        'account_id' => '72e47e47-95b9-41c2-bdcd-55dd9bb04e14',
                        'text' => 'Welcome to Dexatel\'s API',
                        'from' => 'Dexatel',
                        'to' => '17088407149',
                        'channel' => 'SMS',
                        'status' => 'delivered',
                        'segments' => 1,
                        'encoding' => 'GSM-7'
                    ]
                ]
            ])),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $provider->setClient(new Client(['handler' => $handlerStack]));

        $this->assertTrue($provider->compose('test')->setTo('380965272792')->send());
    }

    public function testFailedStatus()
    {
        $provider = new Provider([
            'apiKey' => 'hurrdurr'
        ]);

        $mock = new MockHandler([
            new Response(201, ['Content-Type' => 'application/json'], Json::encode([
                'data' => [
                    [
                        'id' => '2e214e92-4d0f-4ec4-9952-583d086582d6',
                        'account_id' => '72e47e47-95b9-41c2-bdcd-55dd9bb04e14',
                        'text' => 'Welcome to Dexatel\'s API',
                        'from' => 'Dexatel',
                        'to' => '17088407149',
                        'channel' => 'SMS',
                        'status' => 'enroute',
                        'segments' => 1,
                        'encoding' => 'GSM-7'
                    ],
                    [
                        'id' => '2e214e92-4d0f-4ec4-9952-583d086582d6',
                        'account_id' => '72e47e47-95b9-41c2-bdcd-55dd9bb04e14',
                        'text' => 'Welcome to Dexatel\'s API',
                        'from' => 'Dexatel',
                        'to' => '17088407149',
                        'channel' => 'SMS',
                        'status' => 'failed',
                        'segments' => 1,
                        'encoding' => 'GSM-7'
                    ]
                ]
            ])),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $provider->setClient(new Client(['handler' => $handlerStack]));

        $this->assertFalse($provider->compose('test')->setTo('380965272792')->send());
    }

    public function testRejectedStatus()
    {
        $this->expectException(ClientException::class);

        $provider = new Provider([
            'apiKey' => 'hurrdurr'
        ]);

        $mock = new MockHandler([
            new Response(400),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $provider->setClient(new Client(['handler' => $handlerStack]));

        $provider->compose('test')->setTo('380965272792')->send();
    }

    public function testNoApiKey()
    {
        $this->expectException(InvalidConfigException::class);
        $this->expectExceptionMessage('Missing required parameter "apiKey" when instantiating "' . Provider::class . '".');

        new Provider();
    }

    public function testClient()
    {
        $provider = new Provider([
            'apiKey' => 'hurrdurr'
        ]);

        $this->assertInstanceOf(Client::class, $provider->getClient());
    }
}
